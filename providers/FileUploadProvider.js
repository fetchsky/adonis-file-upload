"use strict";

const { ServiceProvider } = use("@adonisjs/fold");

class FileUploadProvider extends ServiceProvider {
  register() {
    this.app.bind("FileUpload", app => {
      return require("../src/FileUpload");
    });
  }
}

module.exports = FileUploadProvider;