"use strict";

const Env = use("Env");
const _ = use("lodash");
const fs = require("fs");
const Drive = use("Drive");
const Config = use("Config");
const bytes = require("bytes");
const Helpers = use("Helpers");
const request = require("request");
const mime = require("mime-types");
const mediaTyper = require('media-typer')


class FileUpload {
  /**
   *
   * @param {String} format
   * @param {String|Object} input
   */
  constructor(format, input) {
    this._format = format;
    this._input = input;
    this._driver = Env.get("DEFAULT_UPLOAD_DISK", Config.get("drive.default"));
  }

  static get FORMAT_URL() {
    return "url";
  }

  static get FORMAT_FILE() {
    return "file";
  }

  set driver(disk) {
    this._driver = disk;
  }

  /**
   *
   * @param {String} [prefix]
   * @param {String} [extenstion]
   * @returns {String}
   */
  static generateUniqueFileName(prefix = null, extenstion = null) {
    var fileName = "";
    if (prefix) {
      fileName = prefix + "_";
    }
    fileName += new Date().getTime() + "_" + _.random(0, 9999);
    if (extenstion) {
      fileName += "." + extenstion;
    }
    return fileName;
  }

  /**
   *
   * @param {Object} validation
   */
  setValidation(validation) {
    this._validation = _.pick(validation, ["types", "size", "extnames"]);
  }

  /**
   * This method is used to validate input
   */
  async validate() {
    if (this._format === FileUpload.FORMAT_FILE) {
      return await this._validateFile();
    } else if (this._format === FileUpload.FORMAT_URL) {
      return await this._validateUrl();
    }
  }

  /**
   * This method is used to upload the file
   *
   * @param {Object} params
   * @param {String} [params.dest]
   * @param {String} [params.fileName]
   * @param {Boolean} [params.unqiueFileName=true]
   * @param {Boolean} [params.cleanOnExit=true]
   */
  async upload(params) {
    if (typeof params.cleanOnExit !== "boolean") {
      params.cleanOnExit = true;
    }
    params.dest = typeof params.dest == "string" ? params.dest : "/";
    if (!_.endsWith(params.dest, "/")) {
      params.dest += "/";
    }
    if (!this.validated) {
      const valid = await this.validate();
      if (!valid) {
        throw new Error("Validation failed");
      }
    }
    const exists = await Drive.disk("local").exists(this._filePath);
    if (!exists) {
      throw new Error("File not found");
    }
    const bufferedContent = await Drive.disk("local").get(this._filePath);
    let fileName;
    if (params.fileName) {
      if (params.unqiueFileName) {
        fileName = FileUpload.generateUniqueFileName(params.fileName, this.extension);
      } else {
        fileName = params.fileName;
      }
    } else {
      fileName = this._fileName;
    }
    const finalPath = params.dest + fileName;
    const uploaded = await Drive.disk(this._driver).put(finalPath, bufferedContent, {
      ContentType: mime.lookup(fileName)
    });
    if (params.cleanOnExit) {
      await this.clean();
    }
    if (!uploaded) {
      throw new Error("Upload failed");
    }
    return {
      fileName,
      fullPath: finalPath,
      extension: this.extension
    };
  }

  /**
   * This method is used to clean file from tmp dir
   */
  async clean() {
    try {
      await Drive.disk("local").delete(this._filePath);
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * This method is used to validate input file
   */
  async _validateFile() {
    this.extension = this._input.extname;
    this._input.setOptions(this._validation);
    this._fileName = FileUpload.generateUniqueFileName(this._input.clientName.replace("." + this.extension, ""), this.extension);
    await this._input.move(Helpers.tmpPath("uploads"), {
      name: this._fileName
    });
    this._filePath = Helpers.tmpPath("uploads/" + this._fileName);
    const valid = this._input.moved();
    if (!valid) {
      this._setError(this._input.error());
    } else {
      this.validated = true;
    }
    return valid;
  }

  /**
   * This method is used to validate input url
   */
  async _validateUrl() {
    this._fileName = FileUpload.generateUniqueFileName();
    this._filePath = Helpers.tmpPath("uploads/" + this._fileName);
    try {
      const resp = await this._downloadFromUrl();
      if (!this._isValidFileType(resp.headers["content-type"]) || !this._isValidFileSize()) {
        return false;
      }
      const ext = mime.extension(resp.headers["content-type"]);
      if (!this._isValidFileExtension(ext+"")) {
        return false;
      }
      this.extension = ext;
      this.validated = true;
      await Drive.disk("local").move(this._filePath, this._filePath + "." + ext);
      this._fileName += "." + ext;
      this._filePath += "." + ext;
      return true;
    } catch (e) {
      this._setError({
        type: "unknown",
        url: this._input,
        message: e.message || "Unknown error"
      });
    }
  }

  /**
   * This method is used to downnload file from url
   */
  async _downloadFromUrl() {
    const file = fs.createWriteStream(this._filePath);
    return new Promise((resolve, reject) => {
      request(this._input)
        .on("error", reject)
        .on("complete", res => {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            resolve(res);
          } else {
            let message = res.statusMessage || 'Unknown error';
            if (res.statusCode) {
              message = 'File resource not found';
            }
            reject(new Error(message));
          }
        })
        .pipe(file);
    });
  }

  /**
   * This method is used to validate content type of file
   *
   * @param {String} type
   * @returns {Boolean}
   */
  _isValidFileType(type) {
    if (!_.isArray(this._validation.types) || !this._validation.types.length) {
      return true;
    }
    let parsedType = mediaTyper.parse(type);
    if (this._validation.types.indexOf(parsedType.type)<0 && this._validation.types.indexOf(parsedType.subtype) <0) {
      let verb = this._validation.types.length === 1 ? 'is' : 'are'
      this._setError({
        type: 'type',
        url: this._input,
        message: `Invalid file type ${parsedType.type} or ${parsedType.subtype}. Only ${this._validation.types.join(', ')} ${verb} allowed`
      });
      return false;
    }
    return true;
  }

  /**
   * This method is used to validate extension of file
   *
   * @param {String} extension
   * @returns {Boolean}
   */
  _isValidFileExtension(extension) {
    if (!_.isArray(this._validation.extnames) || !this._validation.extnames.length) {
      return true;
    }
    if (this._validation.extnames.indexOf(extension) < 0) {
      let verb = this._validation.extnames.length === 1 ? 'is' : 'are'
      this._setError({
        type: 'type',
        url: this._input,
        message: `Invalid file extension ${extension}. Only ${this._validation.extnames.join(', ')} ${verb} allowed`
      });
      return false;
    }
    return true;
  }

  /**
   * This method is used to validate the size of file
   *
   * @returns {Boolean}
   */
  _isValidFileSize() {
    if (!this._validation.size) {
      return true;
    }
    const stats = fs.statSync(this._filePath);
    if (!this._validation._size) {
      if (typeof this._validation.size === "string") {
        this._validation._size = bytes(this._validation.size, {});
      } else {
        this._validation._size = this._validation.size;
      }
    }
    const valid = this._validation._size >= stats.size;
    if (!valid) {
      this._setError({
        type: "size",
        url: this._input,
        message: "File size should be less than " + this._validation.size
      });
    }
    return valid;
  }

  /**
   * This method is used to set validation errors
   *
   * @param {Object} error
   */
  _setError(error) {
    this._error = {
      url: error.url,
      type: error.type,
      message: error.message,
      fieldName: error.fieldName,
      clientName: error.clientName
    };
  }

  /**
   * This method is used to return validation errors
   *
   * @returns {Object}
   */
  errors() {
    return this._error;
  }
}

module.exports = FileUpload;